<?php

	$website = array(
		
			
									
			// FOOTER					
									
					'footer'=>array(
											'company_name'		=>'JUST CHECK',
											'address'			=>'NORTH STREET,COIMBATORE',
											'contact_number'	=>'8508862253',
											'email_id'			=>'JUSTCHECK.com'
                					),			
									
			
			//MENU
					'menu'=>array(
					
							'home'  => array(
												 'text'			=>'HOME',
												 'url'			=>'home',
												 'page_title'	=>'FEATURES',
												 
												 //definition list 
												  'content'		=>[
																	
																	array(
																		'dt'	=>'Site Search & Browse',
																		'dd'	=>'Visitors to online stores have a need for speed - and it starts with time-saving search. Consumers expect online stores to allow them to search for a product and quickly find the product they want. Customers also navigate via categories, along with browsing by price, category and brand distinctions. If your ecommerce shopping cart does not include a search feature for your site, your visitor may simply decide to look elsewhere.'
																	 ),
																array(
																		'dt'	=>'Special Offers',
																		'dd'	=>'Looking for a way to woo new customers - or existing customers you havent seen in a while? Ecommerce shopping carts can help you promote special offers like order discounts, coupon codes, free shipping and gift cards. These offers serve a dual purpose -they encourage customers to take advantage of a "good deal" while also helping you compete in todays competitive online marketplace.'
																	),
																array(
																	    'dt'	=>'A mobile website option',
																		'dd'	=>'Online shoppers need to be offered convenience and instant ease-of-access to your online store, no matter what device they’re using. Your online store should be built on a responsive design template and its features need to be available to all users, at any time, and from anywhere.'
																	 ),
																array(
																	'dt'	=>'Boost your sales with discounts',
																	'dd'	=>'Offer price and quantity discounts. Give away free products. Sell gift vouchers. Send buyers discount coupons to make them come back. Present special prices to members and resellers. Its all there.'
																)
														],
														
														'content_type'=>'dl'
												 
									 		),
											
 							'about_us' => array(
													'text'			=>'ABOUT',
													'url'			=>'about_us',
													'page_title'	=>'WHAT WE OFFER',
																	   
													
													//list
													'content'		=>[
																		
																		array(
																			
																			'li'	=>'Combo offer-Buy 2 items save 5% off wih axis bank buzz credit card'
																			),
																		array(
																			'li'	=>'Frequently bought together-If you buy three product which we offer we will give you 20%discount'
																			),
																		array(
																			'li'	=>'Special offers for,
																						Birthday celebration,
																						weddings
																						'
																	
																			),
																		array(
																			'li'	=>'wedding combo offer-Buy 4 set of combo pack using digibank by flat 100%'
																	
																			),
																		array(
																			'li'	=>'Bulk Gifing Offer-1% Insan Discount on Purchase of More than 1 Just check Email gift card in Order'
																	
																			),
																		array(
																			'li'	=>'flat 50% cashback upto Rs.50 on Prepaid Mobile recharges using Just check'
																	
																			),
																		array(
																			'li'	=>'Extra 15% back on orders above Rs.2000'
																	
																			),
																		array(
																			'li'	=>'Pen Drives & OTG cables starting at Rs.129s'
																	
																			),
																	
																	
																	],
														
														'content_type'=>'li'
			
												),
									
  							'gallery'  =>array(
													'text'			=>'GALLERY',  
													'url'			=>'gallery',
													'page_title'	=>'Gallery',
													'page_content'	=>'Gallry using Division',
													
													'content'		=>array(
																		'image_per_row'	=>'3',
																		'item_header'	=>[
																					   
																					   'product_img' =>array(
																									'title'			=>'item1',
																									'height'		=>'10',
																									'width'			=>'10'
																										),
															
																					   'title'=>array(
																										'title'		=>'item2',
																										'height'		=>'10',
																										'width'			=>'10'
																									),
																						 'description'=>array(
																										'title'		=>'item3',
																										'height'		=>'10',
																										'width'			=>'10'
																										),
																						 ],
																
																		'item_data'		=>[
																						array('<img src="images/3.jpg"></img>','Saree','A perfect blend of classic and contemporary design'),
																						array('<img src="images/4.jpg"></img>','Lehanga','velvet is back in he fashion scene with a bang!he duppatta is a leight color as conrast.'),
																						array('<img src="images/3.jpg"></img>','Anarkali','this multi layered anarkali gown is traditional with contemporary finish.Displaying the best of both ethnic & modern artwork'),
																				
																			
																					],	
																						
																			),
														'content_type'=>'gallery'
													
											),
							'product'  =>array(
													'text'			=>'PRODUCT',  
													'url'			=>'product',
													'page_title'	=>'PRODUCT',
													'page_content'	=>'ghg',
													'content'		=>array(
																			
																		'header'	=>[
																					   
																					   'Name' =>array(
																									'title'	=>'name',
																									'width'		=>'20%'
																									),
																					   'Type'	=>array(
																									'title'	=>'type',
																									'width'		=>'20%'
																									),
															
																					   'Description'=>array(
																										'title'	=>'description',
																										'width'		=>'30%'
																									),
																						 'Price'=>array(
																										'title'	=>'price',
																										'width'		=>'20%'
																										),
																						 'Offer'=>array(
																										'title'	=>'offer',
																										'width'		=>'20%'
																										),
																						 ],
																
																		'data'		=>[
																						array('Saree','cotton','A super quality cotton yarn is mixed with traditional silk to produce kora cotton','Rs.1000','Rs.850'),
																						array('Lehanga','wedding','Made up of pure silk with heavy patterns and designs.heavy embroidry patterns at the borders,broad laces of the lehanga','Rs.10000','Rs.9500'),
																						array('Anarkali','50','this multi layered anarkali gown is traditional with contemporary finish.Displaying the best of both ethnic & modern artwork','Rs.1090','Rs.987')
																				
																			
																					],
																			),
														'content_type'=>'table'
													
											),
							'order'  => array(
												 'text'			=>'ORDER',
												 'url'			=>'order',
												 'page_title'	=>'product order',
												 
												 //definition list 
												  'content'		=>[
																	
																	array(
																		'Name'		=>'Saree',
																		'Price'		=>'Rs.1000',
																		'Image'		=>'<img src="images/1.jpg"></img>',
																		'Description'=>'A super quality cotton yarn is mixed wih traditional silk to produce kora cotton'
																	 ),
																array(
																		'Name'		=>'Lehanga',
																		'Price'		=>'Rs.1500',
																		'Image'		=>'<img src="images/1.jpg"></img>',
																		'Description'=>'Made up of pure silk with heavy patterns and designs.heavy embroidry patterns at the borders,broad laces of the lehanga'
																	),
																
														],
														
														'content_type'=>'div'
												 
									 		),
											
							
					'brands'=>array(
											'text'=>'BRANDS',
											'url'=>'brands',
											'page_title'=>'BRANDS',
											'content'	=>[
														'data'	=>[
				
																'classpolo'=>[
																	'page_title'=>'classpolo',
																	'text'=>'classpolo',
																	'url'=>'classpolo',
																	'sub_con'=>[
																			array(
																					'Name'		=>'Saree',
																					'Price'		=>'Rs.1000',
																					'Image'		=>'<img src="images/1.jpg"></img>',
																					'Description'=>'A super quality cotton yarn is mixed wih traditional silk to produce kora cotton'
																				),
																			array(
																					'Name'		=>'Lehanga',
																					'Price'		=>'Rs.1500',
																					'Image'		=>'<img src="images/1.jpg"></img>',
																					'Description'=>'Made up of pure silk with heavy patterns and designs.heavy embroidry patterns at the borders,broad laces of the lehanga'
																					),
															
																			],			
																			'content_type'=>'div'
										
																		],		
												'max'=>[
														'page_title'=>'Max',
														'text'=>'Max',
														'url'=>'Max',
														'sub_con'=>[
															'header'	=>[
																					   
																	'Name' =>array(
																			   'title'	=>'name',
																			   'width'		=>'20%'
																			   ),
																   'Type'	=>array(
																			   'title'	=>'type',
																			   'width'		=>'20%'
																			   ),
																		   
																	'Description'=>array(
																			   'title'	=>'description',
																			   'width'		=>'30%'
																				   ),
																	'Price'=>array(
																			   'title'	=>'price',
																			   'width'		=>'20%'
																			   ),
																	'Offer'=>array(
																			   'title'	=>'offer',
																			   'width'		=>'20%'
																			   ),
																	],
																
													'data'		=>[
														array('Saree','cotton','A super quality cotton yarn is mixed with traditional silk to produce kora cotton','Rs.1000','Rs.850'),
														array('Lehanga','wedding','Made up of pure silk with heavy patterns and designs.heavy embroidry patterns at the borders,broad laces of the lehanga','Rs.10000','Rs.9500'),
														array('Anarkali','50','this multi layered anarkali gown is traditional with contemporary finish.Displaying the best of both ethnic & modern artwork','Rs.1090','Rs.987')
																				
																			
														],
												], //sub
														
										'content_type'=>'table'		
					
						], //brand2
												
								'adidas'=>[
																	'page_title'=>'adidas',
																	'text'=>'adidas',
																	'url'=>'adidas',
																	'sub_con'=>[
																			array(
																			
																			'li'	=>'Combo offer-Buy 2 items save 5% off wih axis bank buzz credit card'
																			),
																		array(
																			'li'	=>'Frequently bought together-If you buy three product which we offer we will give you 20%discount'
																			),
																		array(
																			'li'	=>'Special offers for,
																						Birthday celebration,
																						weddings
																						'
																	
																			),
																		array(
																			'li'	=>'wedding combo offer-Buy 4 set of combo pack using digibank by flat 100%'
																	
																			),
																		array(
																			'li'	=>'Bulk Gifing Offer-1% Insan Discount on Purchase of More than 1 Just check Email gift card in Order'
																	
																			),
																		array(
																			'li'	=>'flat 50% cashback upto Rs.50 on Prepaid Mobile recharges using Just check'
																	
																			),
																		array(
																			'li'	=>'Extra 15% back on orders above Rs.2000'
																	
																			),
																		array(
																			'li'	=>'Pen Drives & OTG cables starting at Rs.129s'
																	
																			),
																	
																			],			
																			'content_type'=>'li'
										
																		],							
												
				], //data
													
				], //content
						'content_type'=>'multiple'
), //for brands
					'contact_us' =>array(
													'text'			=>'CONTACT US',  
													'url'			=>'contact_us',
													'page_title'	=>'ANY FEEDBACK COMMENT HERE',
													'page_content'	=>'contact',				
													'content'	=>array(
															
																			'cname'	=>array(
																						'label'	=>'Name',
																						'type'	=>'text',
																						'id'	=>'cname',
																						'max_length'=>'32',
																						'mandatory'=>'1'
																					),
																			'email'	=>array(
																						'label'	=>'Email',
																						'type'	=>'text',
																						'id'	=>'email',
																						'max_length'=>'64',
																						'mandatory'=>'1'
																					),
																			'address'	=>array(
																						'label'	=>'address',
																						'type'	=>'textarea',
																						'id'	=>'address',
																						'mandatory'=>'0',
																						'max_length'=>'128',
																						'rows'	=>"0",
																						'cols'	=>"20"
																					),
																			'contact'	=>array(
																						'label'	=>'Contact',
																						'type'	=>'text',
																						'mandatory'=>'1',
																						'max_length'=>'16',
																						'id'	=>'contact'
																					),
																			
																			'product' 		=>array(
																							'label'=>'Dep',
																							'type'=>'select',
																							'mandatory'=>'1',
																							'max_length'=>'32',
																							'id'	=>'product',
																							'op_da'=>[
																								array(
																								    'value'	=>'1',
																									'label'	=>'Womens wear',
																								),
																							  array(
																								    'value'	=>'2',
																									'label'	=>'Mens wear'
																								   ),
																							  array(
																								    'value'	=>'3',
																									'label'	=>'kids wear'
																								   ),
																								   ],																							
																						),
																			
																			'feedback'	=>array(
																						'label'	=>'Feedback',
																						'type'	=>'textarea',
																						'id'	=>'feedback',
																						'mandatory'=>'1',
																						'max_length'=>'128',
																						'rows'	=>"0",
																						'cols'	=>"20"
																					),
																			'submit'	=>array(
																						'label'	=>'submit',
																						'type'	=>'submit',
																						'id'	=>'submit'
																					),
																		
																		),							  
											'content_type'=>'form'
								), //for contact us
					
							'admin' =>array(
													'text'			=>'ADMIN',  
													'url'			=>'admin',
													'page_title'	=>'DATA IN TABLE FORMAT',
													'page_content'	=>'admin',
													'content_type'=>	'display'
										   ),
													
							
				), //for menu
					 
      );  //for $website
?>