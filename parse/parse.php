<?php
	
	//start of definition type
		
		$parse['dl']=function($param){
	
				$lv = array();
			
				$lv['content'] = '';
			 
				foreach($param as $content_idx=>$content_data){				
														
						 $lv['content'].='<dt><font color="red" size="4"><b>'.$content_data['dt'].'</b></font></dt><br><dd><font color="black" size="3"><b>'.$content_data['dd'].'</dd><br></font>';	
										
				} //end of for each 
			
				return  '<dl>'.$lv['content'].'</dl>';
			
		}; //end of definition type
		
	
	//start of li type
	
	
		$parse['li']=function($param){
			
				$lv = array();
			
				$lv['content'] = '';
			 
				foreach($param as $content_idx=>$content_data){				
					
					$lv['content'].='<li><b>'.$content_data['li'].'</b></li><br>';
										
				} //end of content data 
			
				return  '<ul>'.$lv['content'].'</ul>'; 	
		};  //end of li type

	
		//start of table type

		$parse['table']=function($param){
			
					$lv=array();	
					$lv['content'] = '';
					$lv['content'].= '<table border="2">';
					$lv['content'].= '<tr>';
					
					foreach($param['header'] as $header_idx=>$header_value){
						
							$a= $header_value['width'];
							
							$lv['content'].="<td width='$a' >$header_idx</td>";
							
					}
													
													
			//data building			
					$lv['content'].= '</tr>';
																										
					foreach($param['data'] as $datarow_key=>$datarow_value){
																																											
							$lv['content'].='<tr>';
																													
							foreach($datarow_value as $datacol_key=>$datacol_val){
																												
									$lv['content'].="<td >$datacol_val</td>";
																												
							} //end of col
																												
							$lv['content'].= '</tr>';
																							
					} //end of row
					$lv['content'].= '</table>';
																							
					return  $lv['content'];
			
		}; //end of table function

//start of item function	
	
			$parse['gallery']=function($param){
		
				$lv=array();
						
				$lv['content'] = '';
											
				$row= $param['image_per_row'];
																							
				foreach($param['item_header'] as $itemheader_idx=>$itemheader_value){
																							
							$height= $itemheader_value['height'];
							$width=$itemheader_value['width'];
																														
				} //end of item header
															
															
				//item data building
				  $item_row=1;
							
				foreach($param['item_data'] as $item_key=>$item_value){
																											
																																							
					if($item_row%$row==0)
					{
						$lv['content'].="<td height=$height width=$width>".$item_value[0].'<br>'.'<font color="red" size="4"><b>'.$item_value[1].'</b></font><br><font color="blue" size="4"><b>'.$item_value[2]."</b></font></td>";
						$lv['content'].= '<tr>';
																																					
					}else
					{
						$lv['content'].="<td height=$height width=$width>".$item_value[0].'<br>'.'<font color="red" size="4"><b>'.$item_value[1].'</b></font><br><font color="blue" size="4"><b>'.$item_value[2]."</b></font></td>";
					}
																																				
					$item_row++;
																								
					} //end of item data
																		
																											
				return  '<table border="1">'.$lv['content'].'</table>';
		}; //end of item function
	
//start of form function
	
		$parse['form']=function($param){
				
				global $parse;
					
					$lv=array();
							
					$lv['content'] = '';
					
					//$lv['content'] = 'x';
					
					foreach($param as $form_key=>$form_data)
					{
						$lv['content'].="<tr><td><b>$form_key</b></td></tr>";
								
						$lv['content'].=$parse[$form_data['type']]($form_data);
						
					}
					//$lv['content'].='</form>'
																																												
			return  '<form method="POST"><table border="1">'.$lv['content'].'</table></form>';
										
		}; //end of form function
		
		//start of text function		
		$parse['text']=function($param){
			
			$lv=array();
			
			$lv['content'] = '';
			
			foreach($param as $form_key=>$form_data){
						
				if($form_data=="text")
				{
					$lv['content'].= "<tr><td><input id='$param[id]' name='$param[id]'  type='$form_key'  maxlength='$param[max_length]'></td></tr>";								
																								
				}
			}
			return  $lv['content'];
		}; //end of text function

		//start of textarea function
		$parse['textarea']=function($param){
		
		$lv=array();
				
				$lv['content'] = '';
				
				foreach($param as $form_key=>$form_data){
					
					if($form_data=="textarea")
					{	
						$value="textarea";
						
						$lv['content'].= "<tr><td><$value rows='$form_key' cols='$form_key' id='$param[id]' name='$param[id]' maxlength='$param[max_length]'></$value></td></tr>";								
																														
					}
				}
				
				return  $lv['content'];
		}; //end of textarea function
		
		//start of button function
		$parse['button']=function($param){
			
			$lv=array();		
			$lv['content'] = '';
			
					$value=$param['type'];
					$lv['content'].= "<tr><td><input type='$value' id='$param[id]' name='$param[id]' value='$param[label]'></td></tr>";						
					
			
			return  $lv['content'];
		}; //end of button function
		
		$parse['submit']=$parse['button'];
		

//start of select function		
		$parse['select']=function($param){
			
			$lv=array();
			
			$lv['content'] = '';
			
			foreach($param as $form_key=>$form_data){
				
				if($form_data=="select")
				{
					
					$lv['content'].= "<tr><td><select id='$param[id]' name='$param[id]' maxlength='$param[max_length]'>";								
																											
					foreach($param['op_da'] as $idx=>$data)
					{
						$lv['content'].="<option value='$data[value]'>$data[label]</option>";
					}
					$lv['content'].= "</select></td></tr>";
					
				}
			}
			
			return  $lv['content'];
	}; //end of select function

	//start of div type

	$parse['div']=function($param){
			
			$lv=array();	
			$lv['content'] = '';
			
			$lv['content'].="<div  data-class='block'>";
			
			foreach($param as $content_idx=>$content_data){
			
				$lv['content'].="<div  data-class='row'>";
				
				foreach($content_data as $idx=>$data)
				{
				
					$lv['content'].='<font color="red" size="4"><div data-class="left" >'.$idx.":".'</div></font><font color="blue" size="4"><div data-class="right">'.$data.'</div></font>';
				
				}
				$lv['content'].="</div>"; //div row
			
			} //end of for each 
				
			$lv['content'].="</div>"; //div block
			return  '<div data-class="stage">'.$lv['content'].'</div>'; //div stage
		
			
		}; //end of div function
		
		
?>


